# Adwise PHP LateXML Parser

## Prerequisites
This package utilizes "latexml" binary, wich can be found on http://dlmf.nist.gov/LaTeXML/. 
To install binaries on Ubuntu use: ``$ sudo apt-get install latexml``

Make sure php function ``exec()`` is available in your configuration.


## Version
1.1.0

## Convert .TEX files to .XML
```php
$latex = new Adwise\Latexml\Parser();
$latex
	->setDebug(true)
	->setInputDir('./var/input/')
	->setInputFile('*.tex')
	->setOutputDir('./var/output/', true)
	->toXml();
```	

## Convert .XML to .HTML
```php
$latex = new Adwise\Latexml\Parser();
$latex
	->setDebug(true)
	->setInputDir('./var/output/xml/')
	->setInputFile('*.xml')
	->setOutputDir('./var/output/html/', true)
	->setStylesheet('./var/xslt/LaTeXML-html.xsl')
	->toHtml(array("--sourcedirectory ./var/input/")); // Set the source directory for files
```

## Reading log entries
``php
...
$logEntries = $latex->getLogEntries();
``
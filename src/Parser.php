<?php

namespace Adwise\Latexml;

use DOMDocument;
use Symfony\Component\Filesystem\Filesystem;

class Parser
{
    private $debug = false;

    private $inputDir;

    private $outputDir;

    private $outputFilename;

    private $stylesheet;

    private $files = [];

    private $logs = [];

    private $latexToXmlExecutable = 'latexml';

    private $urlPrefix = '';

    /**
     * Checks the basic dependencies to use this library
     */
    public function __construct()
    {
        if (!function_exists('exec') || exec('echo LATEXML') !== 'LATEXML') {
            throw new \Exception('PHP exec() function not available.');
        }
    }

    /**
     * Set debug mode
     *
     * @param $bool
     *
     * @return Parser
     */
    public function setDebug($bool)
    {
        $this->debug = (bool) $bool;

        return $this;
    }

    /**
     * Returns true if debug mode is enabled
     * @return bool
     */
    public function getDebug()
    {
        return $this->debug;
    }

    /**
     * Set the latex file input directory
     *
     * @param $dir
     *
     * @return Parser
     * @throws \Exception
     */
    public function setInputDir($dir)
    {
        if (!is_dir($dir)) {
            throw new \Exception("Input dir {$dir} is not a directory.");
        }

        $this->inputDir = $dir;

        return $this;
    }

    /**
     * Returns the path to input dir
     * @return mixed
     */
    public function getInputDir()
    {
        return $this->inputDir;
    }

    /**
     * @return mixed
     */
    public function getLatexToXmlExecutable()
    {
        return $this->latexToXmlExecutable;
    }

    /**
     * @param mixed $latexToXmlExecutable
     *
     * @return Parser
     */
    public function setLatexToXmlExecutable($latexToXmlExecutable)
    {
        $this->latexToXmlExecutable = $latexToXmlExecutable;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrlPrefix()
    {
        return $this->urlPrefix;
    }

    /**
     * @param mixed $urlPrefix
     *
     * @return Parser
     */
    public function setUrlPrefix($urlPrefix)
    {
        $this->urlPrefix = $urlPrefix;

        return $this;
    }

    /**
     * Set the file(s) to proces. Use *.tex to import all files from input dir
     *
     * @param $file
     *
     * @return Parser
     * @throws \Exception
     */
    public function setInputFile($file)
    {
        $this->files = [];

        if (!$this->getInputDir()) {
            $dir = dirname($file);
            if ($dir === '.') {
                $dir = '.'.DIRECTORY_SEPARATOR;
                $this->setInputDir($dir);
            }
        }

        if (strpos($file, '*') !== false) {
            $files = glob($this->getInputDir().$file);
            if (!empty($files)) {
                $this->files = array_merge($this->files, $files);
            }
        } else {
            if (file_exists($this->getInputDir().$file)) {
                $this->files = array_merge($this->files, [$this->getInputDir().$file]);
            }
        }

        return $this;
    }

    /**
     * Set the xml file output directory
     *
     * @param      $dir
     * @param bool $create
     *
     * @return Parser
     * @throws \Exception
     */
    public function setOutputDir($dir, $create = true)
    {
        if (!is_dir($dir)) {
            if (!$create) {
                throw new \Exception("Output dir {$dir} is not a directory.");
            } else {
                mkdir($dir, 0755, true);
            }
        }
        $this->outputDir = $dir;

        return $this;
    }

    /**
     * Returns the path to output dir
     * @return mixed
     */
    public function getOutputDir()
    {
        return $this->outputDir;
    }

    /**
     * Set the output filename specificly
     *
     * @param $name
     *
     * @return Parser
     */
    public function setOutputFilename($name)
    {
        $this->outputFilename = $name;

        return $this;
    }

    /**
     * Returns the output filename
     * @return mixed
     */
    public function getOutputFilename()
    {
        return $this->outputFilename;
    }

    /**
     * Set XSLT stylesheet
     *
     * @param $stylesheet
     *
     * @return Parser
     * @throws \Exception
     */
    public function setStylesheet($stylesheet)
    {
        if (!file_exists($stylesheet)) {
            throw new \Exception("XSLT stylesheet ({$stylesheet}) is nog a file.");
        }

        $this->stylesheet = $stylesheet;

        return $this;
    }

    /**
     * Returns the stylesheet
     * @return mixed
     */
    public function getStylesheet()
    {
        return $this->stylesheet;
    }

    /**
     * Primary method to convert .tex input files to XML
     *
     * @param array $options
     *
     * @return array
     */
    public function toXml(array $options = [])
    {

        $converted = [];

        if (!empty($this->files)) {
            foreach ($this->files as $inputFile) {

                if ($this->getOutputFilename()) {
                    $name = $this->getOutputFilename();
                } else {
                    $name = pathinfo($inputFile, PATHINFO_FILENAME);
                }

                $outputFile = $this->getOutputDir().$name.'.xml';

                $options = array_merge(
                    [
                        "--path {$this->getInputDir()}",
                    ],
                    $options
                );

                $cmd = $this->getLatexmlCommand($inputFile, $outputFile, $options);

                if ($this->exec($cmd)) {
                    $converted[] = $outputFile;
                }

            }
        }

        return $converted;
    }

    /**
     * Primary method to convert .xml input files to HTML
     *
     * @param array  $options
     * @param string $relativePathPrefix
     *
     * @return array
     */
    public function toHtml(array $options = [], $relativePathPrefix = null)
    {

        $converted = [];

        if (!empty($this->files)) {
            foreach ($this->files as $inputFile) {
                $fileOptions = $options;

                if ($this->getOutputFilename()) {
                    $name = $this->getOutputFilename();
                } else {
                    $name = pathinfo($inputFile, PATHINFO_FILENAME);
                }
                $outputFile = $this->getOutputDir().$name.'.html';

                $fileOptions = array_merge(
                    [
                        "--format html",
                    ],
                    $fileOptions
                );

                if ($this->getStylesheet()) {
                    $options[] = "--stylesheet {$this->getStylesheet()}";
                }

                $cmd = $this->getLatexmlPostCommand($inputFile, $outputFile, $fileOptions);

                if ($this->exec($cmd)) {
                    $this->removeUnwantedElements($outputFile);

                    // Fix relative paths
                    if (null !== $relativePathPrefix) {
                        $this->rewriteRelativePaths($outputFile, $relativePathPrefix);
                    }

                    $converted[] = $outputFile;
                }

            }
        }

        return $converted;
    }

    /**
     * Returns the latexml command to execute
     *
     * @param       $input
     * @param       $output
     * @param array $options
     *
     * @return string
     */
    private function getLatexmlCommand($input, $output, array $options = [])
    {

        $defaults = [
            $this->getLatexToXmlExecutable(),
            '--quiet',
            '--nocomments',
        ];

        $defaults = array_merge($defaults, $options);

        $defaults[] = " --destination {$output}";

        $cmd = implode(" ", $defaults);
        $cmd .= " {$input}";
        $cmd .= " 2>&1";

        return $cmd;
    }

    /**
     * Returns the latexmlpost command to execute
     *
     * @param       $input
     * @param       $output
     * @param array $options
     *
     * @return string
     */
    private function getLatexmlPostCommand($input, $output, array $options = [])
    {

        $defaults = [
            'latexmlpost',
            '--novalidate',
            '--graphicimages',
            '--omitdoctype',
            '--nodefaultresources',
        ];

        $defaults   = array_merge($defaults, $options);
        $defaults[] = " --destination {$output}";

        $cmd = implode(" ", $defaults);
        $cmd .= " {$input}";
        $cmd .= " 2>&1";

        return $cmd;

    }

    /**
     * Runs the exec command and returns false on non-clean exit code
     *
     * @param $cmd
     *
     * @return bool
     */
    private function exec($cmd)
    {
        exec($cmd, $output, $exitcode);

        if ($this->getDebug() && !empty($output)) {
            $this->writeLog($output);
        }

        if ($exitcode === 1) {
            return false;
        }

        return true;
    }

    /**
     * Logs console output to log files directory
     *
     * @param $output
     *
     * @return Parser
     */
    private function writeLog($output)
    {

        if (is_array($output)) {
            $output = implode(PHP_EOL, $output);
        }

        $this->logs[] = $output;

        return $this;

    }

    /**
     * Returns the log entries
     * @return array
     */
    public function getLogEntries()
    {
        $entries    = $this->logs;
        $this->logs = [];

        return $entries;
    }

    /**
     * Remove css link tags
     *
     * @param string $htmlFile
     */
    private function removeUnwantedElements($htmlFile)
    {
        if (!is_file($htmlFile)) {
            return;
        }

        libxml_use_internal_errors(true);
        $dom = new DOMDocument();
        $dom->loadHTML(file_get_contents($htmlFile));

        $elementsToRemove = [];

        foreach ($dom->getElementsByTagName('footer') as $footer) {
            $elementsToRemove[] = $footer;
        }

        foreach ($elementsToRemove as $element) {
            $element->parentNode->removeChild($element);
        }

        $fs = new Filesystem();
        $fs->dumpFile($htmlFile, $dom->saveHTML());

        libxml_use_internal_errors(false);
    }

    /**
     * By default the images are made relative to the html file, which are not accessible for the public
     * Add a prefix to those URL's to make them accesible
     *
     * @param string $htmlFile
     * @param string $relativePathPrefix
     */
    private function rewriteRelativePaths($htmlFile, $relativePathPrefix)
    {
        if (!is_file($htmlFile)) {
            return;
        }

        libxml_use_internal_errors(true);
        $dom = new DOMDocument();
        $dom->loadHTML(file_get_contents($htmlFile));

        foreach ($dom->getElementsByTagName('img') as $img) {
            if (strpos($img->getAttribute('src'), 'data:image') !== 0) {
                $img->setAttribute('src', $this->getUrlPrefix().$relativePathPrefix.'/'.$img->getAttribute('src'));
            }
        }

        $fs = new Filesystem();
        $fs->dumpFile($htmlFile, $dom->saveHTML());

        libxml_use_internal_errors(false);
    }
}
